import React, { useState } from "react";
import "./App.css";
import Header from "./components/Layout/Header";
import Meals from "./components/Meals/Meals";
import Cart from "./components/Cart/Cart";
import CartContextProvider from "./store/CartContextProvider";

function App() {
  const [showCart, setShowCart] = useState(false);

  const cartButtonClickHandler = () => {
    setShowCart(true);
  };

  const closeCartHandler = () => {
    setShowCart(false);
  };

  return (
    <CartContextProvider>
      {showCart && <Cart closeModalHandler={closeCartHandler} />}
      <Header onCartButtonClick={cartButtonClickHandler} />
      <main>
        <Meals />
      </main>
    </CartContextProvider>
  );
}

export default App;
