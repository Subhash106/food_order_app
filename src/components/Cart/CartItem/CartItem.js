import React, { useContext } from "react";

import styles from "./CartItem.module.css";
import CartContext from "../../../store/cart-context";

const CartItem = (props) => {

  const cartContext = useContext(CartContext);

  const addItemHandler = () => {
    cartContext.addItem({id: props.id, name: props.name, price: props.price, qty: 1});
  };

  const removeItemHandler = () => {
    cartContext.removeItem(props.id);
  };

  return (
    <li className={styles["cart-item"]}>
      <div className={styles["item-details"]}>
        <h3>{props.name}</h3>
        <div className={styles["price-details"]}>
          <div className={styles["item-price"]}>$ {props.price}</div>
          <div className={styles["item-qty"]}>x {props.qty}</div>
        </div>
      </div>
      <div className={styles.actions}>
        <button onClick={removeItemHandler}>-</button>
        <button onClick={addItemHandler}>+</button>
      </div>
    </li>
  );
};

export default CartItem;
