import React, { useRef, useState } from "react";

import styles from "./Checkout.module.css";

const Checkout = (props) => {
  const nameInputRef = useRef();
  const emailInputRef = useRef();
  const streetInputRef = useRef();
  const postalCodeInputRef = useRef();
  const cityInputRef = useRef();
  const [formInputsValidity, setFormInputsValidity] = useState({
    name: true,
    email: true,
    street: true,
    postalCode: true,
    city: true,
  });

  const isNotEmpty = (value) => {
    return value.trim() !== "";
  };

  const hasMinimumFiveChars = (value) => {
    return value.trim().length >= 5;
  };

  const includesAt = (value) => {
    return value.includes("@");
  };

  const checkoutHandler = (event) => {
    event.preventDefault();

    const name = nameInputRef.current.value;
    const email = emailInputRef.current.value;
    const street = streetInputRef.current.value;
    const postalCode = postalCodeInputRef.current.value;
    const city = cityInputRef.current.value;

    const nameIsValid = isNotEmpty(name);
    const emailIsValid = includesAt(email);
    const streetIsValid = hasMinimumFiveChars(street);
    const postalCodeIsValid = hasMinimumFiveChars(postalCode);
    const cityIsValid = isNotEmpty(city);

    setFormInputsValidity({
      name: nameIsValid,
      email: emailIsValid,
      street: streetIsValid,
      postalCode: postalCodeIsValid,
      city: cityIsValid,
    });

    const isFormValid =
      nameIsValid &&
      emailIsValid &&
      streetIsValid &&
      postalCodeIsValid &&
      cityIsValid;

    if (!isFormValid) {
      return;
    }

    const data = { name, email, street, postalCode, city };

    props.onConfirm(data);

  };

  return (
    <form className={styles.form} onSubmit={checkoutHandler}>
      <div
        className={`${styles["control"]} ${
          formInputsValidity.name ? "" : styles.invalid
        }`}
      >
        <label htmlFor="name">Your Name</label>
        <input ref={nameInputRef} type="text" id="name" />
        {!formInputsValidity.name && (
          <p className={styles["error-text"]}>Name must not be empty!</p>
        )}
      </div>

      <div
        className={`${styles["control"]} ${
          formInputsValidity.email ? "" : styles.invalid
        }`}
      >
        <label htmlFor="email">Email</label>
        <input ref={emailInputRef} type="text" id="email" />
        {!formInputsValidity.email && (
          <p className={styles["error-text"]}>Email must include '@'!</p>
        )}
      </div>

      <div
        className={`${styles["control"]} ${
          formInputsValidity.street ? "" : styles.invalid
        }`}
      >
        <label htmlFor="street">Street</label>
        <input ref={streetInputRef} type="text" id="street" />
        {!formInputsValidity.street && (
          <p className={styles["error-text"]}>
            Street must be atleast 5 chars long!
          </p>
        )}
      </div>

      <div
        className={`${styles["control"]} ${
          formInputsValidity.postalCode ? "" : styles.invalid
        }`}
      >
        <label htmlFor="postalCode">Postal Code</label>
        <input ref={postalCodeInputRef} type="text" id="postalCode" />
        {!formInputsValidity.postalCode && (
          <p className={styles["error-text"]}>
            Postal code must be 5 chars long!
          </p>
        )}
      </div>

      <div
        className={`${styles["control"]} ${
          formInputsValidity.city ? "" : styles.invalid
        }`}
      >
        <label htmlFor="city">City</label>
        <input ref={cityInputRef} type="text" id="city" />
        {!formInputsValidity.city && (
          <p className={styles["error-text"]}>Name must not be empty!</p>
        )}
      </div>

      <div className={styles.actions}>
        <button type="button" onClick={props.onClick}>
          Close
        </button>
        <button className={styles.submit} type="submit">Confirm</button>
      </div>
    </form>
  );
};

export default Checkout;
