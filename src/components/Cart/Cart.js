import React, { useContext, useState } from "react";

import styles from "./Cart.module.css";
import CartItem from "./CartItem/CartItem";
import Modal from "../UI/Modal/Modal";
import CartContext from "../../store/cart-context";
import Checkout from "./Checkout/Checkout";
import Loader from "../UI/Loader/Loader";

const Cart = (props) => {
  const cartContext = useContext(CartContext);
  const [showCheckout, setShowCheckout] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [submitOrderError, setSubmitOrderError] = useState(null);
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);

  const orderClickHandler = () => {
    setShowCheckout(true);
  };

  const confirmOrderHandler = (userData) => {
    const dataToSubmit = {
      userData,
      totalAmount: cartContext.totalAmount,
      items: cartContext.items,
    };

    const submitOrder = async () => {
      setIsLoading(true);
      const response = await fetch(
        "https://food-order-app-e5c31-default-rtdb.firebaseio.com/orders.json",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(dataToSubmit),
        }
      );

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      setIsLoading(false);

      setIsFormSubmitted(true);

      cartContext.clearCart();
    };

    submitOrder().catch((error) => {
      setIsLoading(false);
      setSubmitOrderError(error.message);
    });
  };

  const modalContent = (
    <React.Fragment>
      <ul className={styles["cart-items"]}>
        {cartContext.items.map((cartItem) => (
          <CartItem
            className={styles["cart-item"]}
            key={cartItem.id}
            id={cartItem.id}
            name={cartItem.name}
            qty={cartItem.qty}
            price={cartItem.price}
          />
        ))}
      </ul>
      <div className={styles.total}>
        <span>Total Amount</span>
        <span>${cartContext.totalAmount}</span>
      </div>

      {isLoading && <Loader />}

      {submitOrderError && <p>{submitOrderError}</p>}

      {showCheckout && (
        <Checkout
          onConfirm={confirmOrderHandler}
          onClick={props.closeModalHandler}
        />
      )}

      {!showCheckout && (
        <div className={styles.actions}>
          <button
            onClick={props.closeModalHandler}
            className={styles["button--alt"]}
          >
            Close
          </button>
          <button onClick={orderClickHandler} className={styles.button}>
            Checkout
          </button>
        </div>
      )}
    </React.Fragment>
  );

  return (
    <Modal onBackdropClick={props.closeModalHandler}>
        {!isFormSubmitted && modalContent}
        {isFormSubmitted && <p style={{textAlign: 'center', fontWeight: 'bold'}}>Order has been placed successfully!</p>}
    </Modal>
  );
};

export default Cart;
