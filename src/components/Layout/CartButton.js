import React, { useContext, useState, useEffect } from "react";

import styles from "./CartButton.module.css";
import CartIcon from "../Cart/CartIcon";
import CartContext from "../../store/cart-context";

const CartButton = (props) => {
  const cartContext = useContext(CartContext);
  const [bumpButton, setBumpButton] = useState(false);

  console.table(cartContext.items);

  const numberOfItems = cartContext.items.reduce((accumulater, item) => {
    return accumulater + item.qty;
  }, 0);

  const btnClasses = `${styles.button} ${bumpButton ? styles.bump : ''}`;

  useEffect(() => {
    if (cartContext.items.length > 0) {
        setBumpButton(true);
    }

    const timer = setTimeout(() => {
        setBumpButton(false);
    }, 300);

    return () => {
        clearTimeout(timer);
    }

  }, [cartContext.items])

  return (
    <React.Fragment>
      <button onClick={props.onClick} className={btnClasses}>
        <span className={styles.icon}>
          <CartIcon />
        </span>
        <span>Your Cart</span>
        <span className={styles.badge}>{numberOfItems}</span>
      </button>
    </React.Fragment>
  );
};

export default CartButton;
