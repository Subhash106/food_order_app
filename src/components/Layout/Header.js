import React from "react";

import styles from "./Header.module.css";
import mealsImage from "../../assets/meals.jpg";
import CartButton from "./CartButton";

const Header = (props) => {
  return (
    <React.Fragment>
      <header className={styles.header}>
        <h1>React Meals</h1>
        <CartButton onClick={props.onCartButtonClick} />
      </header>
      <div className={styles["main-image"]}>
        <img alt="meal img" src={mealsImage} />
      </div>
    </React.Fragment>
  );
};

export default Header;
