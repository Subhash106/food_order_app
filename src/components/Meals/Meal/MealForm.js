import React, { useContext, useRef, useState } from "react";

import styles from "./MealForm.module.css";
import Input from "../../UI/Input/Input";
import CartContext from "../../../store/cart-context";

const MealForm = (props) => {
  const cartContext = useContext(CartContext);
  const inputRef = useRef();
  const [isFormValid, setIsFormValid] = useState(true);

  const formSubmitHandler = (event) => {
    event.preventDefault();
    const enteredInput = inputRef.current.value;

    const enteredInputNumber = +enteredInput;

    if (
      enteredInput.trim().length === 0 ||
      enteredInputNumber < 1 ||
      enteredInputNumber > 5
    ) {
      setIsFormValid(false);
      return;
    }

    const itemToAdd = props.item;

    itemToAdd.qty = enteredInputNumber;

    cartContext.addItem(itemToAdd);
  };

  return (
    <form className={styles.form} onSubmit={formSubmitHandler}>
      <Input
        ref={inputRef}
        label="Qty"
        input={{
          type: "number",
          id: "amount" + props.id,
          min: "1",
          max: "5",
          step: "1",
          defaultValue: "1",
        }}
      />
      {!isFormValid && <p>Amount should be a valid number betbeen 1 to 5.</p>}
      <button>+ Add</button>
    </form>
  );
};

export default MealForm;
