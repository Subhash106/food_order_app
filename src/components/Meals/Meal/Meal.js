import React from "react";
import styles from "./Meal.module.css";
import MealForm from "./MealForm";

const Meal = (props) => {
    const price = `$${props.price.toFixed(2)}`;
  return (
    <div className={styles.meal}>
      <div>
        <h3>{props.name}</h3>
        <div className={styles.description}>{props.description}</div>
        <div className={styles.price}>{price}</div>
      </div>
      <div>
          <MealForm item={props.item} id={props.id} />
      </div>
    </div>
  );
};

export default Meal;
