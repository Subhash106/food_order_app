import React, { useState, useEffect } from "react";
import styles from "./AvailableMeals.module.css";
import Meal from "./Meal/Meal";
import Card from "../UI/Card/Card";
import Loader from "../UI/Loader/Loader";

const AvailableMeals = () => {
  const [meals, setMeals] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [httpError, setHttpError] = useState(null);

  useEffect(() => {
    const fetchMeals = async () => {
      const response = await fetch(
        "https://food-order-app-e5c31-default-rtdb.firebaseio.com/meals.json"
      );

      if (!response.ok) {
        throw new Error("Something went wrong!");
      }

      const data = await response.json();

      const formatedMeals = [];

      for (const key in data) {
        formatedMeals.push({
          id: key,
          name: data[key].name,
          description: data[key].description,
          price: data[key].price,
        });
      }

      setMeals(formatedMeals);

      setIsLoading(false);
    };

    fetchMeals().catch((error) => {
      setIsLoading(false);
      setHttpError(error.message);
    });
  }, [httpError]);

  return (
    <Card className={styles.meals}>
      {!isLoading &&
        !httpError &&
        meals.map((meal) => (
          <Meal
            item={meal}
            id={meal.id}
            key={meal.id}
            name={meal.name}
            description={meal.description}
            price={meal.price}
          />
        ))}
      {isLoading && !httpError && <Loader />}
      {httpError && <p style={{textAlign: 'center', fontWeight: 'bold'}}>{httpError}</p>}
    </Card>
  );
};

export default AvailableMeals;
