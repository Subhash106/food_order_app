import CartContext from "./cart-context";
import { useReducer } from "react";

const cartReducer = (initialState, actions) => {
  switch (actions.type) {
    case "ADD_ITEM":
      const itemIndex = initialState.findIndex(
        (item) => item.id === actions.item.id
      );

      const item = initialState[itemIndex];
      const updatedState = [...initialState];

      if (item) {
        const updatedItem = { ...item };
        updatedItem["qty"] += actions.item.qty;
        updatedState[itemIndex] = updatedItem;

        return updatedState;
      } else {
        return initialState.concat(actions.item);
      }
    case "REMOVE_ITEM":
      const index = initialState.findIndex(
        (element) => element.id === actions.id
      );

      const existingItem = initialState[index];
      const stateCopy = [...initialState];

      if (existingItem) {
        const existingItemCopy = { ...existingItem };

        if (existingItemCopy.qty > 1) {
          existingItemCopy["qty"] -= 1;
          stateCopy[index] = existingItemCopy;
        } else {
          stateCopy.splice(index, 1);
        }
      }
      return stateCopy;
    case "CLEAR_CART":
      return [];
    default:
      return initialState;
  }
};

const CartContextProvider = (props) => {
  const defaultState = [];
  const [items, setItemsDispatchFn] = useReducer(cartReducer, defaultState);

  const addItem = (item) => {
    setItemsDispatchFn({ type: "ADD_ITEM", item: item });
  };

  const removeItem = (id) => {
    setItemsDispatchFn({ type: "REMOVE_ITEM", id: id });
  };

  const clearCart = () => {
      setItemsDispatchFn({type: 'CLEAR_CART'});
  }

  const totalAmount = items.reduce((accumulater, item) => {
    return accumulater + item.price * item.qty;
  }, 0);

  return (
    <CartContext.Provider
      value={{
        items: items,
        totalAmount: totalAmount,
        addItem: addItem,
        removeItem: removeItem,
        clearCart: clearCart
      }}
    >
      {props.children}
    </CartContext.Provider>
  );
};

export default CartContextProvider;
