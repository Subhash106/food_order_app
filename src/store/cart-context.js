import React from 'react';

const CartContext = React.createContext({
  items: [],
  totalAmount: null,
  addItem: (item) => {},
  removeItem: (id) => {},
  clearCart: () => {},
});

export default CartContext;
